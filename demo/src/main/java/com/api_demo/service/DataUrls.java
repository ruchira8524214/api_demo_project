package com.api_demo.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class DataUrls {
    public StringBuffer getResponseData() throws IOException{
        String url = "https://api.unsplash.com/photos/random?client_id=CHx2EuDtn3VjnzzEwRrY9htyC2xVrr3eho1O4avu3ng";

        HttpURLConnection httpClient = (HttpURLConnection) new URL(url).openConnection();
        httpClient.setRequestMethod("GET");

        StringBuffer response = new StringBuffer();
        int responseCode = httpClient.getResponseCode();

        if(responseCode == HttpURLConnection.HTTP_OK){
            BufferedReader br = new BufferedReader(new InputStreamReader(httpClient.getInputStream()));
            String inputLine;

            while((inputLine = br.readLine()) != null){
                response.append(inputLine);
            }
            br.close();
            return response;
        }else{
            throw new RuntimeException("GET request failed with response code " + responseCode);
        }
    }
}
