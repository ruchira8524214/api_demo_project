package com.api_demo.service;

import org.json.JSONObject;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class APIService extends Application{

        static String imageUrl = "";

        public static void imageData() throws Exception{
            StringBuffer response = new DataUrls().getResponseData();
            if(response!=null){
                JSONObject obj = new JSONObject(response.toString());
                JSONObject urlObj = obj.getJSONObject("urls");
                imageUrl = urlObj.getString("small_s3");
            }else{
                System.out.println("Response is empty");
            }
        }
        public void start(Stage primaryStage) throws Exception{
            imageData();

            Image img = new Image(imageUrl);
            ImageView imgView = new ImageView(img);
            Pane imgPane = new Pane();
            imgPane.getChildren().addAll(imgView);

            Scene sc = new Scene(imgPane, img.getWidth(), img.getHeight());
            primaryStage.setScene(sc);
            primaryStage.setTitle("ImageView Example");
            primaryStage.show();
        }
}
